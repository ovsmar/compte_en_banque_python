from datetime import datetime
from colors import bcolors
import pprint
class BankAccount:
    """
    BankAccount Class
    """
    def __init__(self, name: str):
        self.id = 123
        self.name = name
        self.balance = 0
        self.decouvertAutorise= -200

    def deposit(self, amount: float) -> bool:
        '''adds money to balance'''
        if amount > 0:
            self.balance += amount 
            return True 
            
        return False

    def withdraw(self, amount: float) -> bool:
        a = self.balance- amount 
        '''soustrait de l'argent du solde'''
        if a < self.decouvertAutorise :
            return False
        self.balance -= amount
        return True

    def display_balance(self):
        """ affiche le solde du compte """
        print(f'\n{bcolors.OKGREEN} Solde disponible est ${self.balance}{bcolors.ENDC}')

    def getCompte(self):
        return {
            "id": self.id,
            "nom": self.name,
            "solde": self.balance,
            "decouvertAutorise": self.decouvertAutorise,
            "timeStamp":datetime.now()
        }

def interface():
    """
   Interface pour interagir avec le compte bancaire
    """
    print('=================================================================')
    print(f'{bcolors.OKGREEN} Bonjour! Bienvenue sur la machine de dépôt et de retrait |{name}|{bcolors.ENDC}')
    print('=================================================================')
   
    while True:
        print('\n∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞')
        action = input  ('\n Souhaitez-vous? \n 1. Déposer  \n 2. Retirer \n 3. Afficher balance  \n 4. Sortir   \n 5. GET COMPTE  ').upper()
        print('\n∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞∞')
        if action in "12345":

            if action == "1":
                try:
                    deposit_amount = float(input(f'\n{bcolors.OKBLUE} Combien souhaitez-vous déposer?: {bcolors.ENDC} '))
                    if not account.deposit(deposit_amount):
                        print(f'\n{bcolors.FAIL} Saisir un nombre positif {bcolors.ENDC}')
                    else:
                        print(f'\n{bcolors.OKGREEN} Avec succès déposé ${deposit_amount} sur votre compte {bcolors.ENDC}') 
                        print(account.display_balance())
                except ValueError:
                   print(f'\n{bcolors.FAIL} Error! Saisir un nombre positif {bcolors.ENDC}')

            if action == "2":
                try:
                    withdraw_amount = float(input(f'\n{bcolors.OKBLUE} Combien souhaitez-vous retirer?: {bcolors.ENDC}'))
                    if not  account.withdraw(withdraw_amount):
                       print(f'\n{bcolors.FAIL} Vous n`avez pas assez d`argent pour retirer. {bcolors.ENDC}')
                       print(account.display_balance())
                    else:
                        print(f'\n{bcolors.OKGREEN} Avec succès retiré ${deposit_amount} depuis votre compte {bcolors.ENDC}')
                        print(account.display_balance())
                except ValueError:
                    print(f'\n{bcolors.FAIL} Error! Saisir un nombre positif {bcolors.ENDC}')

            if action == "3":
                account.display_balance()

            if action == "4":
                print (f'\n{bcolors.HEADER} Thank you, good bye! {bcolors.ENDC}')
                break
                
            
            if action == "5":
                dict = account.getCompte()
                pprint.pprint(dict)


if __name__ == '__main__':
    name = input(f'\n {bcolors.OKBLUE} Quel est votre prénom?{bcolors.ENDC} ')
    account = BankAccount(name)
    # print(account.getCompte() == account.getCompte())
    interface()